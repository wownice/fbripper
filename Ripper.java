package FBRipper.FBRipper;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;

import io.github.bonigarcia.wdm.FirefoxDriverManager;

public class Ripper
{	
	private Map<String, String> elements;
	
	private WebDriver driver;
	
	public Ripper()
	{				
		loadElementValues();
	}
	
	public void start(String email, String pass, String messengerLink, int ripCount)
	{
		FirefoxDriverManager.getInstance().setup();
		driver = new FirefoxDriver();
		driver.get("https://www.facebook.com/");
		login(email, pass);
		driver.get(messengerLink);
			
		String messengerBoxXpath = "//div[@class='" + elements.get("ele_messengerBox_CLASS") + "']";
				
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(messengerBoxXpath)));	
		
		rip(ripCount);
	}
	
	public void login(String email, String pass)
	{
		WebElement Ele = driver.findElement(By.id(elements.get("ele_email_ID")));
		Ele.sendKeys(email);
		Ele = driver.findElement(By.id(elements.get("ele_pass_ID")));
		Ele.sendKeys(pass);
		Ele = driver.findElement(By.id(elements.get("ele_loginButton_ID")));
		Ele.click();		
	}
	
	public void loadElementValues()
	{
		elements = new HashMap<String, String>();
		File configFile = new File("config.txt");
		if(!configFile.exists())
		{
			createConfig(configFile);
		}
		else
		{
			readConfig(configFile);
		}
	}
	
	public void createConfig(File config)
	{
		elements.put("ele_email_ID", "email");
		elements.put("ele_pass_ID", "pass");
		elements.put("ele_loginButton_ID", "u_0_2");
		elements.put("ele_messengerBox_CLASS", "_1t_p clearfix");//the entirety of messenger
		elements.put("ele_messageGroup_CLASS", "_41ud"); //each group of messages
		elements.put("ele_userMessageString_CLASS", "clearfix _o46 _3erg _3i_m _nd_ direction_ltr text_align_ltr"); //each of the user's message's box
		elements.put("ele_otherMessageString_CLASS", "clearfix _o46 _3erg _29_7 direction_ltr text_align_ltr"); //each of the other speaker's message's box
		elements.put("ele_scrollElement_CLASS", "_2k8v"); //the hidden div that shows the loading circle when scrolling up
		elements.put("ele_scrollElementInner_CLASS", "_3u55 _3qh2 img sp_PB1AI5W6lNB sx_ec425e"); // i ONLY -- the content inside the scrollElement div. Shows when loading, disappears (Both visually and in HTML) when not		
		elements.put("ele_imageLink_CLASS", "_4tsk"); //the div tag that contains the <a> tag image link (_4tsl is the <a>'s class)
		
		BufferedWriter writeFile;
		try 
		{
			config.createNewFile();
			writeFile = new BufferedWriter(new FileWriter(config));
			for(Map.Entry<String, String> entry : elements.entrySet())
			{	//elementName:elementLocator;
				writeFile.write(entry.getKey() + ":" + entry.getValue() + ";");
			}
			writeFile.close();
		}
		catch (IOException e) {e.printStackTrace();}		
	}	
	
	public void updateConfig(Map<String, String> ele)
	{
		File config = new File("config.txt");
		BufferedWriter writeFile;
		try 
		{
			config.createNewFile();
			writeFile = new BufferedWriter(new FileWriter(config));
			for(Map.Entry<String, String> entry : ele.entrySet())
			{	//elementName:elementLocator;
				writeFile.write(entry.getKey() + ":" + entry.getValue() + ";");
			}
			writeFile.close();
			elements = ele;
		}
		catch (IOException e) {e.printStackTrace();}
	}
	
	public void readConfig(File config)
	{
		try 
		{
			BufferedReader readFile = new BufferedReader(new FileReader(config));
			String[] semicolonSplit = readFile.readLine().split(";");
			for(String s : semicolonSplit)
			{
				String[] individual = s.split(":");
				elements.put(individual[0], individual[1]);
			}
			readFile.close();
		}
		catch (IOException e) {e.printStackTrace();}
	}
	
	public void rip(int ripCount)
	{	
		try
		{
			WebElement loadingElement = driver.findElement(By.xpath("//div[@class='" + elements.get("ele_scrollElement_CLASS") + "']"));		
			if(ripCount < 0) //will save all messages (scrolling all the way up)
			{
				while(loadingElement != null)
					messengerScroll(loadingElement);
			}
			else
			{
				for(int i = 0; i < ripCount; i++)
				{					
					messengerScroll(loadingElement);
				}
			}
		} catch(StaleElementReferenceException e){} catch(NoSuchElementException e1){} //if scroll element is gone, terminate
		
		try 
		{
			File ripFile = new File(generateFileName(ripCount));
			ripFile.createNewFile();
			BufferedWriter writeFile;
			try 
			{
				writeFile = new BufferedWriter(new FileWriter(ripFile));
				List<WebElement> messageGroup = driver.findElements(By.xpath("//div[@class='" + elements.get("ele_messageGroup_CLASS") +"']")); //messages are displayed in groups at time		
				
				String writeString;
				String dateString;
				int imgCount = 1;
				
				for(WebElement group : messageGroup)
				{			
					List<WebElement> messages = group.findElements(By.xpath(".//div[@class='" + 
				elements.get("ele_userMessageString_CLASS") + 
				"' or @class='" + elements.get("ele_otherMessageString_CLASS") +
				"']"));
					for(WebElement mess : messages)
					{
						dateString = mess.findElement(By.xpath(".//div")).getAttribute("data-tooltip-content");
						if(dateString == null) //if is an image, go down 2 divs instead
						{
							dateString = mess.findElement(By.xpath(".//div/div")).getAttribute("data-tooltip-content");
						}
						//speaker(date):message
						writeString = group.findElement(By.xpath(".//h5")).getAttribute("textContent") + 
								"(" + dateString + "): ";
						try 
						{
							List<WebElement> images = mess.findElements(By.xpath(".//a[@class = '" + elements.get("ele_imageLink_CLASS") + "']"));
							for(WebElement img : images)
							{
								//img.getAttribute("href")
								/*	//what would've worked if webP could be ripped
								writeString += "<Image " + imgCount + "/>";
								URL imgURL = new URL(img.getAttribute("href")); //files are saved in webP format, wont work
							    BufferedImage bufferImg = ImageIO.read(imgURL);
							    File file = new File("Image " + imgCount + ".png");
							    ImageIO.write(bufferImg, "png", file);
							    */
								writeString += "<Image" + imgCount + ">" + img.getAttribute("href") + "</Image" + imgCount + ">";
								imgCount++;
							}
						}
						catch(NoSuchElementException e1){}
						writeString += mess.getText();
						writeFile.write(writeString);						
						writeFile.newLine();
					}
				}				
				writeFile.close();
			}
			catch (IOException e2) {e2.printStackTrace();}		
		} catch (IOException e) {e.printStackTrace();}
	}	

	public void messengerScroll(WebElement loadElement)
	{
		((JavascriptExecutor) driver).executeScript("document.getElementsByClassName('" + elements.get("ele_scrollElement_CLASS") + "')[0].scrollIntoView(true);", loadElement);			
		loadElement.sendKeys(Keys.PAGE_UP);
		loadLoop(loadElement);
		loadElement = driver.findElement(By.xpath("//div[@class='" + elements.get("ele_scrollElement_CLASS") + "']"));
	}
	
	public void loadLoop(WebElement loadElement)
	{
		try 
		{
			Thread.sleep(1000);
			if(loadElement.findElement(By.xpath("//i[@class='" + elements.get("ele_scrollElementInner_CLASS") + "']")) != null)
			{
				loadLoop(loadElement);
			}
        } catch (StaleElementReferenceException e) {} catch(NoSuchElementException e1) {}catch (InterruptedException e) {}
	}
	
	public String generateFileName(int count)
	{
		String fileName = "Messenger " + count + "_" + Calendar.MONTH + "_" + Calendar.DAY_OF_MONTH + "_" + Calendar.YEAR + ".txt";
		File tempFile = new File(fileName);
		int counter = 0;
		while(tempFile.exists())
		{
			fileName = "Messenger " + count + "_" + Calendar.MONTH + "_" + Calendar.DAY_OF_MONTH + "_" + Calendar.YEAR + "_" + counter + ".txt";
			tempFile = new File(fileName);
		}
		return fileName;
	}
	
	public Map<String, String> getElements()
	{
		return elements;
	}
}

