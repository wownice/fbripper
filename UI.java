package FBRipper.FBRipper;
import java.util.HashMap;
import java.util.Map;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class UI 
{
	private Ripper ripper;
	private Stage stage;
	
	//rip tab
	private TextField userEmail;
	private TextField userPass;
	private TextField messengerLink;
	private TextField ripCount;
	
	//config tab
	private TextField emailElement;
	private TextField passElement;
	private TextField loginButtonElement;
	private TextField messengerBoxElement;
	private TextField messageGroupElement;
	private TextField userMessageStringElement;
	private TextField otherMessageStringElement;
	private TextField scrollElement;
	private TextField scrollElementInner;
	private TextField imageLinkElement;
		
	public UI(Stage gloStage)
	{
		stage = gloStage;
		ripper = new Ripper();
		initializeUI();
	}
	
	public void initializeUI()
	{	
		/*					Rip Tab					*/
		HBox emailHBox = new HBox();
		Label emailLabel = new Label("Email");
		emailLabel.setAlignment(Pos.CENTER);
		emailLabel.setMinWidth(90);
		userEmail = new TextField();
		emailHBox.getChildren().addAll(emailLabel, userEmail);
		
		HBox passHBox = new HBox();
		Label passLabel = new Label("Password");
		passLabel.setAlignment(Pos.CENTER);
		passLabel.setMinWidth(90);
		userPass = new PasswordField();
		passHBox.getChildren().addAll(passLabel, userPass);
		
		HBox linkHBox = new HBox();
		messengerLink = new TextField();
		Label messengerLinkLabel = new Label("Messenger Link");
		messengerLinkLabel.setAlignment(Pos.CENTER);
		messengerLinkLabel.setMinWidth(90);
		messengerLinkLabel.setTooltip(new Tooltip("The link to the desired chat log to rip from."));
		linkHBox.getChildren().addAll(messengerLinkLabel, messengerLink);
		
		HBox ripCountHBox = new HBox();
		ripCount = new TextField();
		Label ripCountLabel = new Label("Rip Count");
		ripCountLabel.setAlignment(Pos.CENTER);
		ripCountLabel.setMinWidth(90);
		ripCountLabel.setTooltip(new Tooltip("The number of message groups to rip from. Groups are denoted by the small timestamps above them."));
		ripCountHBox.getChildren().addAll(ripCountLabel, ripCount);
		
		Button ripButton = new Button("Rip");
		ripButton.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				ripper.start(userEmail.getText(), userPass.getText(), messengerLink.getText(), Integer.parseInt(ripCount.getText()));
			}
		});

		VBox ripBox = new VBox();
		ripBox.setAlignment(Pos.CENTER);
		ripBox.getChildren().addAll(emailHBox, passHBox, linkHBox, ripCountHBox, ripButton);
		
		Tab ripTab = new Tab("Rip");
		ripTab.setContent(ripBox);
		/*					Rip Tab					*/
		
		/*					Config Tab					*/
		Map<String, String> elements = ripper.getElements();

		HBox emailElementHBox = new HBox();
		Label emailElementLabel = new Label("Email Id");
		emailElementLabel.setMinWidth(120);
		emailElementLabel.setAlignment(Pos.CENTER);
		emailElement = new TextField();
		emailElement.setText(elements.get("ele_email_ID"));
		emailElementHBox.getChildren().addAll(emailElementLabel, emailElement);
		
		HBox passElementHBox = new HBox();
		Label passElementLabel = new Label("Pass Id");
		passElementLabel.setMinWidth(120);
		passElementLabel.setAlignment(Pos.CENTER);
		passElement = new TextField();
		passElement.setText(elements.get("ele_pass_ID"));
		passElementHBox.getChildren().addAll(passElementLabel, passElement);
		
		HBox loginButtonElementHBox = new HBox();
		Label loginButtonElementLabel = new Label("Login Button Id");
		loginButtonElementLabel.setMinWidth(120);
		loginButtonElementLabel.setAlignment(Pos.CENTER);
		loginButtonElement = new TextField();
		loginButtonElement.setText(elements.get("ele_loginButton_ID"));
		loginButtonElementHBox.getChildren().addAll(loginButtonElementLabel, loginButtonElement);
		
		HBox messengerBoxElementHBox = new HBox();
		Label messengerBoxElementLabel = new Label("Messenger Class");
		messengerBoxElementLabel.setMinWidth(120);
		messengerBoxElementLabel.setAlignment(Pos.CENTER);
		messengerBoxElement = new TextField();
		messengerBoxElement.setText(elements.get("ele_messengerBox_CLASS"));
		messengerBoxElementHBox.getChildren().addAll(messengerBoxElementLabel, messengerBoxElement);
		
		HBox messageGroupElementHBox = new HBox();
		Label messageGroupElementLabel = new Label("Message Group Class");
		messageGroupElementLabel.setMinWidth(120);
		messageGroupElementLabel.setAlignment(Pos.CENTER);
		messageGroupElement = new TextField();
		messageGroupElement.setText(elements.get("ele_messageGroup_CLASS"));
		messageGroupElementHBox.getChildren().addAll(messageGroupElementLabel, messageGroupElement);
		
		HBox userMessageStringElementHBox = new HBox();
		Label userMessageStringElementLabel = new Label("User Message Class");
		userMessageStringElementLabel.setMinWidth(120);
		userMessageStringElementLabel.setAlignment(Pos.CENTER);
		userMessageStringElement = new TextField();
		userMessageStringElement.setText(elements.get("ele_userMessageString_CLASS"));
		userMessageStringElementHBox.getChildren().addAll(userMessageStringElementLabel, userMessageStringElement);
		
		HBox otherMessageStringElementHBox = new HBox();
		Label otherMessageStringElementLabel = new Label("Friend Message Class");
		otherMessageStringElementLabel.setMinWidth(120);
		otherMessageStringElementLabel.setAlignment(Pos.CENTER);
		otherMessageStringElement = new TextField();
		otherMessageStringElement.setText(elements.get("ele_otherMessageString_CLASS"));
		otherMessageStringElementHBox.getChildren().addAll(otherMessageStringElementLabel, otherMessageStringElement);
		
		HBox scrollElementHBox = new HBox();
		Label scrollElementLabel = new Label("Load Icon Class");
		scrollElementLabel.setMinWidth(120);
		scrollElementLabel.setAlignment(Pos.CENTER);
		scrollElement = new TextField();
		scrollElement.setText(elements.get("ele_scrollElement_CLASS"));
		scrollElementHBox.getChildren().addAll(scrollElementLabel, scrollElement);
		
		HBox scrollElementInnerHBox = new HBox();
		Label scrollElementInnerLabel = new Label("Inner Load Icon Class");
		scrollElementInnerLabel.setMinWidth(120);
		scrollElementInnerLabel.setAlignment(Pos.CENTER);
		scrollElementInner = new TextField();
		scrollElementInner.setText(elements.get("ele_scrollElementInner_CLASS"));
		scrollElementInnerHBox.getChildren().addAll(scrollElementInnerLabel, scrollElementInner);

		HBox imageLinkElementHBox = new HBox();
		Label imageLinkElementLabel = new Label("Image Link Class");
		imageLinkElementLabel.setMinWidth(120);
		imageLinkElementLabel.setAlignment(Pos.CENTER);
		imageLinkElement = new TextField();
		imageLinkElement.setText(elements.get("ele_imageLink_CLASS"));
		imageLinkElementHBox.getChildren().addAll(imageLinkElementLabel, imageLinkElement);
		
		Button saveConfigButton = new Button("Save");
		saveConfigButton.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				Map<String, String> ele = new HashMap<String, String>();
				ele.put("ele_email_ID", emailElement.getText());
				ele.put("ele_pass_ID", passElement.getText());
				ele.put("ele_loginButton_ID", loginButtonElement.getText());
				ele.put("ele_messengerBox_CLASS", messengerBoxElement.getText());
				ele.put("ele_messageGroup_CLASS", messageGroupElement.getText());
				ele.put("ele_userMessageString_CLASS", userMessageStringElement.getText());
				ele.put("ele_otherMessageString_CLASS", otherMessageStringElement.getText());
				ele.put("ele_scrollElement_CLASS", scrollElement.getText());
				ele.put("ele_scrollElementInner_CLASS", scrollElementInner.getText());
				ele.put("ele_imageLink_CLASS", imageLinkElement.getText());
				ripper.updateConfig(ele);
			}
		});
		
		VBox configBox = new VBox();
		configBox.setAlignment(Pos.CENTER);
		configBox.getChildren().addAll(
				emailElementHBox, passElementHBox, loginButtonElementHBox, messengerBoxElementHBox,
				messageGroupElementHBox, userMessageStringElementHBox, otherMessageStringElementHBox, 
				scrollElementHBox, scrollElementInnerHBox, imageLinkElementHBox, saveConfigButton);
		ScrollPane configScroll = new ScrollPane(configBox);
		configScroll.setMaxHeight(600);
		configScroll.setHbarPolicy(ScrollBarPolicy.NEVER);
		Tab configTab = new Tab("Configuration");
		configTab.setContent(configScroll);
		/*					Config Tab					*/
		
		TabPane tabs = new TabPane();
		tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		tabs.getTabs().setAll(ripTab, configTab);
		Scene scene = new Scene(tabs);
		stage.getIcons().add(new Image("img/BeastBuster.png"));
		stage.setTitle("FB Ripper");
		stage.setScene(scene);
		stage.show();
	}
}
